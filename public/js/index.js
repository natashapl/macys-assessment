async function getBodyHTML() {
  let promise = await fetch("../test_feed.json");
  let data = await promise.json();
  let wordCountArray = [];
  let timeStampArray = [];
  const chartContainer = document.getElementById("myChart");

  for (let i = 0; i < data.content.length; i++) {
    let bodyHTML = data.content[i].content.bodyHtml;
    let unixTimeStamp = data.content[i].content.createdAt;
    let time = new Date(unixTimeStamp * 1000);
    let hours = time.getHours();
    let minutes = (time.getMinutes() < 10 ? "0" : "") + time.getMinutes();
    let formattedTime = hours + ":" + minutes;

    if (bodyHTML !== undefined) {
      let words = bodyHTML.split(">").pop();
      let wordCount = words.trim().split(/\s+/).length;

      wordCountArray.push(wordCount);
      timeStampArray.push(formattedTime);
    }
  }

  const dataChart = new Chart(chartContainer, {
    type: "bar",
    data: {
      labels: timeStampArray,
      datasets: [
        {
          label: "Word Count",
          data: wordCountArray,
          backgroundColor: ["rgba(54, 162, 235, 0.2)"],
          borderColor: ["rgba(54, 162, 235, 1)"],
          borderWidth: 1,
        },
      ],
    },
    options: {
      scales: {
        y: {
          beginAtZero: true,
        },
      },
    },
  });
}

getBodyHTML();
